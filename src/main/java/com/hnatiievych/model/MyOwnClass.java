package com.hnatiievych.model;

public class MyOwnClass {
    @MyAnnotation
    private int age;
    @MyAnnotation
    private String name;
    private boolean isHuman;
    private String note;

    private void showAge(int age){
        System.out.println("Age is: "+ getAge());
    }

    private String getSomeInfo(String name){
        return ("name of MyOwnclass is: " + name);
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHuman(boolean human) {
        isHuman = human;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public String getNote() {
        return note;
    }
    private void myMethodInt(String a, int ... args){
        System.out.println(a+args.length);
    }

    private void myMethodString(String...arg){
        System.out.println(arg.toString());
    }
}
