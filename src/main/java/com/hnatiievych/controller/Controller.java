package com.hnatiievych.controller;

import com.hnatiievych.model.MyAnnotation;
import com.hnatiievych.model.MyOwnClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Controller {

    private static Logger logger = LogManager.getLogger(Controller.class);

    public void printFieldWithAnnotation() {
        Class clazz = MyOwnClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                String name = myAnnotation.name();
                int age = myAnnotation.age();
                logger.info(field.getName());
            }
        }
    }


    public void printAnnotationValueInConsole() {
        Class clazz = MyOwnClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                String name = myAnnotation.name();
                int age = myAnnotation.age();
                logger.info("name in my annotation is: " + name);
                logger.info("age in my annotation is: " + age);
            }
        }
    }

    public void invokeMethod() {
        MyOwnClass ownClass = new MyOwnClass();
        Class clazz = MyOwnClass.class;
        try {
            Method method1 = clazz.getDeclaredMethod("showAge", int.class);
            method1.setAccessible(true);
            method1.invoke(ownClass, 30);
            Method method2 = clazz.getDeclaredMethod("getSomeInfo", String.class);
            method2.setAccessible(true);
            logger.info(method2.invoke(ownClass, "Petruchho"));
            Method method3 = clazz.getDeclaredMethod("getNote");
            logger.info(method3.invoke(ownClass));
        } catch (NoSuchMethodException | IllegalAccessException |
                InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    public void setValue() {
        MyOwnClass object = new MyOwnClass();
        Class clazz = MyOwnClass.class;
        Field[] fields = clazz.getDeclaredFields();
        fields[0].setAccessible(true);
        if (fields[0].getType() == int.class) {
            try {
                fields[0].setInt(object, 18);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }


    public void invokeMyMethod() {
        MyOwnClass myOwnClass = new MyOwnClass();
        Class clazz = myOwnClass.getClass();
        try {
            Method method1 = clazz.getDeclaredMethod("myMethodInt", String.class, int[].class);
            method1.setAccessible(true);
            int[] argsInt = new int[]{1, 2, 3, 4, 5};
            method1.invoke(myOwnClass, argsInt);

            Method method2 = clazz.getDeclaredMethod("myMethodString", String[].class);
            method2.setAccessible(true);
            String[] strings = {"one", "two", "three"};
            method2.invoke(myOwnClass, strings);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }


    public void showInfo() {
        Object object = new Object();
        Class clazz = object.getClass();
        logger.info("The name of class is: " + clazz.getName());
        logger.info("The name of class is: " + clazz.getSimpleName());
        Constructor constructor = null;
        try {
            constructor = clazz.getConstructor();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        logger.info("The constructor of class is: " + constructor.getName());
        logger.info("The declare method of class is: ");
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            logger.info(method.getName());
        }
        logger.info("The declare fields of class is: ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            logger.info(field.getName());
        }
    }
}
