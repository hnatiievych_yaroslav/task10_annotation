package com.hnatiievych.view;

public interface Printable {
    void print();
}
