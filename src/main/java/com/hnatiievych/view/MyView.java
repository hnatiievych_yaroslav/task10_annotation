package com.hnatiievych.view;

import com.hnatiievych.Application;
import com.hnatiievych.controller.Controller;
import com.hnatiievych.model.MyAnnotation;
import com.hnatiievych.model.MyOwnClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(Application.class);

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Task2.Print field with annotation");
        menu.put("2", "  2 - Task3.Print annotation value into console");
        menu.put("3", "  3 - Task4. Invoke method with different parameters");
        menu.put("4", "  4 - Task5. Set value into field not knowing its type");
        menu.put("5", "  5 - Task6. Invoke myMethod(String a, int ... args) and myMethod(String … args)");
        menu.put("6", "  6 - Task7. show all information about that Class.");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        logger.info("Task2.Print field with annotation");
        controller.printFieldWithAnnotation();
    }

    private void pressButton2() {
        logger.info("Task3.Print annotation value into console");
        controller.printAnnotationValueInConsole();
    }

    private void pressButton3() {
        logger.info("Task4. Invoke method with different parameters");
        controller.invokeMethod();

    }

    private void pressButton4() {
        logger.info("Task5. Set value into field not knowing its type");
        controller.setValue();

    }

    private void pressButton5() {
        logger.info("Task6. Invoke myMethod(String a, int ... args) and myMethod(String … args)");
        controller.invokeMyMethod();
    }

    private void pressButton6() {
        logger.info("Task7. show all information about that Class.");
        controller.showInfo();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
